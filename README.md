# Searching

My implementations of search algos in c++

- Linear Search 
- Binary Search
- Jump Search
- Interpolation Search
- Exponential Search
- Ternary Search